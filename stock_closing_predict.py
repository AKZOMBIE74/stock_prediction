if __name__ == '__main__':
    # Importing Libraries
    import numpy as np
    import matplotlib.pyplot as plt
    import pandas as pd
    from pandas.plotting import scatter_matrix
    np.warnings.filterwarnings('ignore')

    x_vars = {
        "open":1,
        "high":2,
        "low":3,
        "closing_price": 4,
        #use close/open
    }
    y_vars = {
        "opening_val": 6
    }

    chevronLoc = "yahoo_gas/CVX.csv"
    proPetro = "yahoo_gas/PUMP.csv"
    exxon = "yahoo_gas/XOM.csv"

    print "START READING"

    dataChevron = pd.read_csv(chevronLoc)
    dataProPetro = pd.read_csv(proPetro)
    dataExxon = pd.read_csv(exxon)

    print "ENDED READING"

    #X-values alternate
    #chevronOpeningPricesX = [dataChevron.iloc[:, [x_vars["open"],x_vars["high"],x_vars["low"]]].values[i] for i in xrange(58)]
    chevronOpeningPricesX = [dataChevron.iloc[:, [x_vars["open"]]].values[i] for i in xrange(58)]
    proPetroOpeningPricesX = [dataProPetro.iloc[:, [x_vars["open"]]].values[i] for i in xrange(58)]
    exxonOpeningPricesX = [dataExxon.iloc[:, [x_vars["open"]]].values[i] for i in xrange(58)]

    #X-values
    chevronClosingPercentagesY = [
        np.array(float(dataChevron.iloc[:, [x_vars["closing_price"]]].values[i]) / float(dataChevron.iloc[:, [x_vars["open"]]].values[i])) for i
        in xrange(58)]
    proPetroClosingPercentagesY = [
        np.array(float(dataProPetro.iloc[:, [x_vars["closing_price"]]].values[i]) / float(dataProPetro.iloc[:, [x_vars["open"]]].values[i])) for i
        in xrange(58)]
    exxonClosingPercentagesY = [
        np.array(float(dataExxon.iloc[:, [x_vars["closing_price"]]].values[i]) / float(dataExxon.iloc[:, [x_vars["open"]]].values[i])) for i
        in xrange(58)]

    #Plot
    #plot1 = pd.DataFrame(data=chevronOpeningPricesX+chevronClosingPercentagesX, columns=["Open","Percent"])
    #axes = scatter_matrix(plot1, alpha=0.2, figsize=(10, 10), diagonal='kde')
    #corr = plot1.corr().as_matrix()

    #plt.show()

    print "START TRAINING"
    #Train Model
    from sklearn.linear_model import LassoCV,ElasticNet
    #from sklearn.datasets import make_regression

    clf = LassoCV()
    clf.fit(X=chevronOpeningPricesX+proPetroOpeningPricesX+exxonOpeningPricesX, y=chevronClosingPercentagesY + proPetroClosingPercentagesY + exxonClosingPercentagesY)

    print "END TRAINING"

    print "Slope: " + str(clf.coef_)
    print "Intercept: " + str(clf.intercept_)

    #Just a quick test prediction using an x-value from chevron that was not used to train the model
    print "PREDICTION"
    print clf.predict(125.040001)
    #Actual value: 0.9972009037
    #Prediction: 1.00286023